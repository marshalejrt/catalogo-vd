# Catalogo Online Vanesa Duran

> Proyecto para mostrar catalogo online y hacer pedidos interactivos

![Version](https://img.shields.io/badge/version-0.1-green.svg)
![Scope](https://img.shields.io/badge/Laravel-5.7-blue.svg)
![Scope](https://img.shields.io/badge/PHP-7.2-blue.svg)
![Status](https://img.shields.io/badge/status-success-green.svg)
![Scope](https://img.shields.io/badge/scope-private-red.svg)
![Dependencies](https://img.shields.io/badge/dependencies-ok-green.svg)

Laravel es un framework de aplicación web con una sintaxis expresiva y elegante. Creemos que el desarrollo debe ser una experiencia agradable y creativa para ser verdaderamente satisfactoria. Laravel intenta eliminar el dolor del desarrollo facilitando las tareas comunes que se utilizan en la mayoría de los proyectos web, como la autenticación, el enrutamiento, las sesiones, la cola de espera y el almacenamiento en caché.

Laravel es accesible, pero potente, proporcionando herramientas necesarias para aplicaciones grandes y robustas. Una magnífica inversión del contenedor de control, sistema de migración expresiva y soporte de pruebas unitarias estrechamente integradas le brindan las herramientas que necesita para construir cualquier aplicación con la que esté encargado.

## Documentación Oficial

Puede encontrar más información en  [Laravel website](http://laravel.com/docs).


## Licencia

Laravel es un software de código abierto con licencia [MIT license](http://opensource.org/licenses/MIT).

## Introducción

Por favor lea la documentación antes de bajar el repositorio y realizar cambios en el mismo.
A continuación encontrará instrucciones para configurar su equipo con el ambiente de trabajo necesario,
como así también una descripción de las tareas más comunes.

Del mismo modo encontrará una descripción de los requisitos de documentación y formatos utilizados para
realizar commits, entre otras tareas.

## Setup Inicial

### En primer lugar debe crear una carpeta en donde alojar los archivos del proyecto, y clonar el repositorio

> Creamos una carpeta en donde se alojarán todos los proyectos que deseamos estén disponibles en el servidor Homestead.
> Dicha carpeta la llamaremos ~/catalogo-vd

```
cd ~
mkdir catalogo-vd
```

#### Clonamos el repositorio
Descargamos el código del proyecto desde el repositorio.
No olvidarse reemplazar __USERNAME__ por su nombre de usuario de BitBucket.

```
cd ~/gumsup
git clone https://USERNAME@bitbucket.org/marshalejrt/catalogo-vd.git
cd ~/catalogo-vd
```

Una vez finalizado este proceso tendrá el código disponible en la carpeta `~/catalogo-vd/catalogo-vd`

### En segundo lugar debe instalar los seguir los siguientes pasos para configurar el proyecto

```
  	Configurar parametros de conexion en el archivo .env, crearlo si no existe
```
```
  	Crear la base de datos en el servidor
```
```
	Instalar Composer en la app  `composer install`
```
```
	Instalar Componentes npm en la app  `npm install`
```
```
	Compilar los archivos del front  `npm run dev or npm run production`
```
```	
	Migrar los Datos `php artisan migrate`
```
```		
	Agregar datos predeterminados con Seed `php artisan db:seed`
```  
```			
	Ejecutar la llave `php artisan key:generate`
```  
```			
	Opcional `composer dump-autoload`
```  
```			
	acceder a la aplicación desde: `http://localhost:8000` 
```  
```			
	User: admin@prueba.com
	Pass: 123456 
```  