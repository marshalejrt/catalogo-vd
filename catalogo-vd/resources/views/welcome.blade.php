<!DOCTYPE html>
<html>

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        Catalogo - Vanesa Duran </title>
    <!-- css -->
    {{--<link href="{{asset('css/app.css')}}" rel="stylesheet">--}}
    <link href="{{asset('css/mdl.css')}}" rel="stylesheet">
    {{--<script src="{{asset('js/app.js')}}"></script>--}}
    <script src="{{asset('js/mdl.js')}}"></script>
</head>

<body>

<div class="container">
    <div class="content">
        <div class="modal fade" id="flip-book-window" tabindex="-1" role="dialog" aria-labelledby="headerLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <div class="modal-body">
                        <div class="mount-node">
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="books">
            <h2>Vanesa Duran</h2>
            <div class="thumb">
                <img id="orwell1984" src="images/orwell1984.jpg" class="btn" alt="George Orwell - 1984"/>
                <div class="caption">
                    Prueba Catalogo Online Vanesa Duran en modo HTML
                </div>
            </div>

        </div>


        <script type="text/javascript">

            function theKingIsBlackPageCallback(n) {
                return {
                    type: 'image',
                    src: 'books/image/theKingIsBlack/' + (n + 1) + '.jpg',
                    interactive: false
                };
            }

            function orwell1984PageCallback(n) {
                return {
                    type: 'html',
                    src: 'books/html/html/' + (n + 1) + '.html',
                    interactive: true
                };
            }

            function previewPageCallback(n) {
                return {
                    type: 'html',
                    src: 'books/html/preview/' + (Math.floor(n / 2) % 3 + 1) + '.html',
                    interactive: true
                };
            }

            var template = {
                html: 'templates/default-book-view.html',
                styles: [
                    'css/font-awesome.min.css',
                    'css/short-white-book-view.css'
                ],
                script: 'js/default-book-view.js'
            };

            var booksOptions = {
                orwell1984: {
                    pageCallback: orwell1984PageCallback,
                    pages: 6,
                    propertiesCallback: function (props) {
                        props.page.depth /= 2;
                        props.cover.padding = 0.002;
                        return props;
                    },
                    template: template
                },
                preview: {
                    pageCallback: previewPageCallback,
                    pages: 200,
                    propertiesCallback: function (props) {
                        props.cover.padding = 0.003;
                        props.sheet.color = 0x008080;
                        return props;
                    },
                    template: template
                },
                theKingIsBlack: {
                    pageCallback: theKingIsBlackPageCallback,
                    pages: 40,
                    propertiesCallback: function (props) {
                        props.cover.color = 0x000000;
                        return props;
                    },
                    template: template
                },
                condoLiving: {
                    pdf: 'books/pdf/CondoLiving.pdf',
                    downloadURL: 'books/pdf/CondoLiving.pdf',
                    template: template
                },
                theThreeMusketeers: {
                    pdf: 'books/pdf/TheThreeMusketeers.pdf',
                    propertiesCallback: function (props) {
                        props.page.depth /= 3;
                        props.cover.padding = 0.002;
                        props.cover.binderTexture = 'books/pdf/binder/TheThreeMusketeers.jpg';
                        return props;
                    },
                    template: template
                },
            };

            var instance = {
                scene: undefined,
                options: undefined,
                node: $('#flip-book-window').find('.mount-node')
            };
            $('#flip-book-window').on('hidden.bs.modal', function () {
                instance.scene.dispose();
            });
            $('#flip-book-window').on('shown.bs.modal', function () {
                instance.scene = instance.node.FlipBook(instance.options);
            });

            $('.books').find('img').click(function (e) {
                if (e.target.id) {
                    instance.options = booksOptions[e.target.id];
                    $('#flip-book-window').modal('show');
                }
            });

        </script>
    </div>

</div>


</body>

</html>
