<?php

use Illuminate\Database\Seeder;

class Tipo_objetosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $tipo_objetos = [
            [
                'descripcion' => 'Imagen'
            ],
            [
                'descripcion' => 'Zona'
            ],
            [
                'descripcion' => 'Video'
            ]
        ];
        foreach ($tipo_objetos as $tipo) {
            $newTipos = \App\Tipo_objetos::where('descripcion', '=', $tipo['descripcion'])->first();
            if ($newTipos === null) {
                $newTipos = \App\Tipo_objetos::create([
                    'descripcion'          => $tipo['descripcion'],
                ]);
            }
        }

        $allTipo_objetos = \App\Tipo_objetos::All();
        foreach ($allTipo_objetos as $tipo) {
            $tipo->save();
        }
    }
}
