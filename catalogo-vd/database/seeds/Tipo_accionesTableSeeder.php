<?php

use Illuminate\Database\Seeder;

class Tipo_accionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $tipo_acciones = [
            [
                'descripcion' => 'Imagen'
            ],
            [
                'descripcion' => 'Zona'
            ],
            [
                'descripcion' => 'Video'
            ],
            [
                'descripcion' => 'Shop'
            ],
            [
                'descripcion' => 'Shop'
            ]
        ];
        foreach ($tipo_acciones as $tipo) {
            $newTipos = \App\Tipo_acciones::where('descripcion', '=', $tipo['descripcion'])->first();
            if ($newTipos === null) {
                $newTipos = \App\Tipo_acciones::create([
                    'descripcion'          => $tipo['descripcion'],
                ]);
            }
        }

        $allTipo_acciones = \App\Tipo_acciones::All();
        foreach ($allTipo_acciones as $tipo) {
            $tipo->save();
        }
    }
}
