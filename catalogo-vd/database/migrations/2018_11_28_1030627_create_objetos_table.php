<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjetosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objetos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_objeto_id')->unsigned()->index();
            $table->foreign('tipo_objeto_id')->references('id')->on('tipo_objetos')->onDelete('cascade');
            $table->float('x');
            $table->float('y');
            $table->float('width');
            $table->float('height');
            $table->string('css');
            $table->string('id_objeto');
            $table->integer('accion_id')->unsigned()->index();
            $table->foreign('accion_id')->references('id')->on('acciones')->onDelete('cascade');
            $table->integer('catalogo_id')->unsigned()->index();
            $table->foreign('catalogo_id')->references('id')->on('catalogos')->onDelete('cascade');
            $table->integer('pagina');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objetos');
    }
}
