const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
var nodeDir = 'node_modules/';

mix.js('resources/js/app.js', 'public/js')
    .scripts([
        // nodeDir + 'jquery/dist/jquery.min.js',
        'resources/js/libraries/jquery.min.js',
        'resources/js/libraries/bootstrap.min.js',
        'resources/js/libraries/html2canvas.min.js',
        'resources/js/libraries/three.min.js',
        'resources/js/libraries/pdf.min.js',
        'resources/js/libraries/3dflipbook.min.js',
        'resources/js/libraries/script.js',
    ], 'public/js/mdl.js')
    .styles([
        'resources/css/styles/bootstrap.min.css',
        'resources/css/style/bootstrap-theme.min.css',
        'resources/css/styles/style.css',
    ], 'public/css/mdl.css')
    .copyDirectory('resources/fonts/', 'public/fonts')
    .copyDirectory('resources/css/', 'public/css/')
    .copyDirectory('resources/js/custom', 'public/js/')
    .copyDirectory('resources/books', 'public/books/')
    .copyDirectory('resources/images', 'public/images/')
    .copyDirectory('resources/sounds', 'public/sounds/')
    .copyDirectory('resources/templates', 'public/templates/')
   .sass('resources/sass/app.scss', 'public/css')
    .autoload({
        jQuery: 'jquery',
        $: 'jquery',
        jquery: 'jquery',
    });
